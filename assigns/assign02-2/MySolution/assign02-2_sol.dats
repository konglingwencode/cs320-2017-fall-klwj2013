(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign02-2_sol_dats
// How to compile:
// myatscc assign02-2_sol.dats
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
#include "./../assign02-2.dats"

staload M = "libats/libc/SATS/math.sats"
staload _(*M*) = "libats/libc/DATS/math.dats"

typedef
x = $tup(double,double)



extern
fun
myfile_mean(myfile:myfile): double

implement
myfile_mean(myfile)=
let
    val $tup(line_num,total)=
    myfile_foldleft<x>(myfile,$tup(0.0,0.0),lam(result,lines)=> 
    if lines = "" then $tup(result.0,result.1)
    else $tup(result.0+1.0, result.1+ g0string2float_double(lines)))
in
    total/line_num
end

typedef
y = $tup(double,double,double)
extern
fun
myfile_stdev(myfile:myfile):double


implement
myfile_stdev(myfile)=
let
    val $tup(sum1,sum2,line_num) = 
    myfile_foldleft<y>(myfile, $tup(0.0,0.0,0.0),lam(result,lines) => 
    if lines ="" then $tup(result.0,result.1,result.2)
    else $tup(result.0 + g0string2float_double(lines), result.1 + g0string2float_double(lines)*g0string2float_double(lines),result.2 +1.0))
in
    $M.sqrt((line_num*sum2-sum1*sum1)/(line_num*line_num))
end

(* ****** ****** *)



(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val
myfile = myfile_open("./Makefile")
val _0_ =
myfile_foldleft
(myfile, 0, lam(_, line) => let val () = println!(line) in 0 end)
//
val
myfile = myfile_open("./DATA/numbers.txt")
val _0_ =
myfile_foldleft
(myfile, 0, lam(_, line) => let val () = println!(line) in 0 end)
//
val
myfile = myfile_open("./DATA/numbers.txt")
val tally =
myfile_foldleft<int>(myfile, 0, lam(res, line) => res + g0string2int(line))
//
val ((*void*)) = println! ("tally = ", tally)
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [assign02-2_sol.dats] *)
